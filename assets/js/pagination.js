//active cho phần mục lục
var currentPage = 1;
var pagination = document.querySelectorAll(".number-pagination");
pagination[1].closest("li").classList.add("active");
pagination[pagination.length - 1].closest("li").classList.add("active");
for (var i = 0; i < pagination.length; i++) {
    pagination[i].addEventListener("click", function(event) {
        event.preventDefault();
        var k = document.querySelectorAll(".pagination-product .active");
        for (var j = 0; j < k.length; j++) {
            k[j].classList.remove("active");
        }
        if (this.innerText === "<") {
            if (currentPage - 1 > 0) {
                currentPage = currentPage - 1;
            }
        } else if (this.innerText === ">") {
            if (currentPage + 1 < pagination.length - 1) {
                currentPage = currentPage + 1;
            }
        } else {
            currentPage = this.innerText;
        }
        pagination[currentPage].closest("li").classList.add("active");

        if (currentPage == 1) {
            pagination[0].closest("li").classList.remove("active");
            pagination[pagination.length - 1].closest("li").classList.add("active");
        } else if (currentPage == pagination.length - 2) {
            pagination[0].closest("li").classList.add("active");
            pagination[pagination.length - 1]
                .closest("li")
                .classList.remove("active");
        } else {
            pagination[0].closest("li").classList.add("active");
            pagination[pagination.length - 1].closest("li").classList.add("active");
        }
    });
}